const { execSync } = require("child_process");
const config = require("config");

console.log('process', process)

const ROOT_DIRECTORY = "../deploy";
const MAIN = config.get("MAIN");
const ROOT = config.get("ROOT");

const deployProject = (workDir) => {
  try {
    process.chdir(`./${workDir}/`);
    execSync(`echo "CLUB_NAME=${MAIN.CLUB_NAME}" >> .env`);
    execSync(`echo "CLUB_INITIALS=${MAIN.CLUB_INITIALS}" >> .env`);
    // execSync("sudo docker-compose up --build -d");
  } catch (error) {
    console.log("error on deployProject: ", error);
  }
};

(async () => {
  deployProject(ROOT_DIRECTORY);
  console.log(ROOT_DIRECTORY, MAIN, ROOT);
})();
