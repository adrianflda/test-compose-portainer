FROM node:8.15.0-alpine

RUN mkdir -p /usr/src/project

RUN mkdir -p /usr/src/deploy

WORKDIR /usr/src/project

ADD package.json .

RUN npm install

ADD . .

CMD [ "npm", "start" ]